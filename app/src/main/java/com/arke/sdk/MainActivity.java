package com.arke.sdk;

import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.transition.Slide;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Toast;
import com.arke.sdk.demo.SimplePayDemo;
import com.arke.sdk.view.Refund;
import com.arke.sdk.view.scanQR;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import timber.log.Timber;


/**
 * The activity is used to manage all test modules.
 */

public class MainActivity extends ExpandableListActivity {

    private static final String TAG = "MainActivity";
    private static final String MODULE = "MODULE";
    private static final String FUNCTION = "FUNCTION";
    private static final int SIMPLE_PAY_INDEX = 0;

    /**
     * Alert dialog.
     */
    private AlertDialog dialog;

    /**
     * Toast.
     */
    private Toast toast;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        setContentView(R.layout.activity_main);

        // Init toast
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        // Init alert dialog
        dialog = new AlertDialog.Builder(this)
                .setNegativeButton(getString(R.string.cancel), null)
                .setCancelable(false)
                .create();

        hideSoftKeyboard();
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    protected boolean allowDisableSystemButton() {
        return true;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (allowDisableSystemButton()) {
            Window win = getWindow();
            try {
                Class<?> cls = win.getClass();
                final Class<?>[] PARAM_TYPES = new Class[]{int.class};
                Method method = cls.getMethod("addCustomFlags", PARAM_TYPES);
                method.setAccessible(true);
                method.invoke(win, new Object[]{0x00000001});
            } catch (Exception e) {
                // handle the error here.
                Timber.e(e.getCause());
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            getApplication().onTerminate();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


    public void gotoTransactions(View view) {
        try {
            SimplePayDemo.getInstance(getApplicationContext(), toast, dialog).execute("Sale");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    public void activate_refund(View v) {
        try {
            SimplePayDemo.getInstance(getApplicationContext(), toast, dialog).execute("Refund");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    public void activate_cashback(View v) {
        try {
            SimplePayDemo.getInstance(getApplicationContext(), toast, dialog).execute("Cashback");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    public void moreTransactions(View v) {
        setContentView(R.layout.navigation_main);
    }

    public void activate_QRScanner(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        Intent intent = new Intent(this, scanQR.class);
        startActivity(intent);
    }

    public void activate_reverse(View v) {

    }

    public void activate_balance(View v) {

    }

    public void activate_e_wallet(View v) {

    }


    public void homePage(View v) {
        setContentView(R.layout.activity_main);
    }



    public void cancelTransactions(View view) {
        Toast.makeText(getApplicationContext(),"Home Screen",Toast.LENGTH_SHORT).show();
    }


}
