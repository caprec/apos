package com.arke.sdk.util.transaction;

/**
 * Terminal info.
 */

public class TerminalInfo {

    public static String merchantName = "FNB MERCHANT";

    public static String merchantNo = "1234567890";

    public static String terminalNo = "12345678";

    public static String copyType = "Customer Copy";

    public static String transStatus = "APPROVED";

    public static String operatorNo = "01";

    public static String issuer = "CUP";

    public static String acquirer = "CCB";
}
