package com.arke.sdk.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arke.sdk.R;
import com.arke.sdk.api.ScannerForBack;
import com.arke.sdk.api.ScannerForFront;
import com.usdk.apiservice.aidl.scanner.OnScanListener;


public class scanQR extends BaseActivity implements View.OnClickListener {
    //View Objects
    private Button buttonScan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_scan_qr );
        buttonScan = (Button) findViewById( R.id.buttonScan );
        buttonScan.setOnClickListener( this );
    }

    public void navigate_other_nav(View v) {
        super.onBackPressed();
    }

    private static final String TAG = "";

    private void startBackScan() throws RemoteException {
        ScannerForBack.getInstance().startScan(30, new OnScanListener.Stub() {

            @Override
            public void onSuccess(String code) throws RemoteException{
                Log.d(TAG, "--- onSuccess ---");
                try{
                    TextView qrCodepay = (TextView) findViewById( R.id.qr_info );
                    qrCodepay.setText(String.valueOf(code));
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(int error) throws RemoteException {
                final TextView qrCode_pay = (TextView) findViewById( R.id.qr_info );
                qrCode_pay.setText(String.valueOf(error));
            }

            @Override
            public void onTimeout() throws RemoteException {

            }

            @Override
            public void onCancel() throws RemoteException {            }
        });
    }

    @Override
    public void onClick(View view) {
        try {
            startBackScan();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}